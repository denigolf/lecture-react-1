import React, { Component } from 'react';
import Preloader from '../Preloader/Preloader';
import Header from '../Header/Header';
import MessageList from '../MessageList/MessageList';
import MessageInput from '../MessageInput/MessageInput';
import { v4 as uuidv4 } from 'uuid';
import './Chat.scss';

class Chat extends Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: true,
      messages: null,
    };

    this.sendMessageHandler = this.sendMessageHandler.bind(this);
    this.deleteMessage = this.deleteMessage.bind(this);
    this.editMessage = this.editMessage.bind(this);
  }

  async componentDidMount() {
    const url = this.props.url;
    const response = await fetch(url);
    const data = await response.json();
    if (data.length === 0) {
      this.setState({ loading: true, messages: null });
      return;
    }
    this.setState({ loading: false, messages: data });
  }

  sendMessageHandler(text, clearInput) {
    if (!text.trim()) {
      alert('Message cannot be an empty string!');
      return;
    }
    const messages = [...this.state.messages];
    const userId = 'my-id';
    let currentTime = new Date();
    currentTime = currentTime.toJSON();
    messages.push({
      id: uuidv4(),
      userId: userId,
      text: text,
      own: true,
      createdAt: currentTime,
    });
    clearInput();
    this.setState({ messages });
  }

  deleteMessage(id) {
    const messages = [...this.state.messages];
    const index = messages.findIndex((message) => {
      return message.id === id;
    });
    messages.splice(index, 1);
    this.setState({ messages });
  }

  editMessage(id) {
    let value = document.querySelector('.message-input-text').value;
    // do {
    //   value = prompt('Enter new message:', '');
    // } while (!value);

    const messages = [...this.state.messages];
    const index = messages.findIndex((message) => {
      return message.id === id;
    });
    messages[index] = { ...messages[index], text: value };
    this.setState({ messages });
  }

  render() {
    const { loading, messages } = this.state;

    return loading ? (
      <Preloader />
    ) : (
      <div className="chat">
        <Header messages={messages} />
        <MessageList
          messages={messages}
          deleteMessage={this.deleteMessage}
          editMessage={this.editMessage}
        />
        <MessageInput
          data={this.state.messages}
          sendMessageHandler={this.sendMessageHandler}
        />
      </div>
    );
  }
}

export default Chat;
