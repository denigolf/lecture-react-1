import React, { Component } from 'react';
import { formatDateHeader } from '../../helpers';
import './Header.scss';

class Header extends Component {
  constructor(props) {
    super(props);

    this.getParticipantsCount = this.getParticipantsCount.bind(this);
    this.getMessagesCount = this.getMessagesCount.bind(this);
    this.getLastMessageTime = this.getLastMessageTime.bind(this);
  }

  getParticipantsCount() {
    const users = [];
    this.props.messages.forEach((message) => {
      const userId = message.userId;
      if (!users.includes(userId)) {
        users.push(userId);
      }
    });

    return users.length;
  }

  getLastMessageTime() {
    const messages = this.props.messages;
    const lastMessage = messages[messages.length - 1];
    const lastMessageTime = lastMessage.createdAt;
    return lastMessageTime;
  }

  getMessagesCount() {
    return this.props.messages.length;
  }

  render() {
    return (
      <header className="header">
        <div className="header-info">
          <div className="header-title">Ozark</div>
          <div className="header-users-count-container">
            <span className="header-users-count">
              {this.getParticipantsCount()}
            </span>
            <span> participants</span>
          </div>
          <div className="header-messages-count-container">
            <span className="header-messages-count">
              {this.getMessagesCount()}
            </span>
            <span>
              {this.getMessagesCount() === 1 ? ' message' : ' messages'}
            </span>
          </div>
        </div>
        <div className="header-last-message-date-container">
          <span>last message </span>
          <span className="header-last-message-date">
            {formatDateHeader(this.getLastMessageTime())}
          </span>
        </div>
      </header>
    );
  }
}

export default Header;
