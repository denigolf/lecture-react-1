import React, { Component } from 'react';
import { formatDateMessage } from '../../helpers';

import './OwnMessage.scss';

class OwnMessage extends Component {
  render() {
    const { message, deleteMessage, editMessage } = this.props;

    return (
      <div className="own-message">
        <div className="message-text-container">
          <span className="message-text">{message.text}</span>
          <span className="message-time">
            {formatDateMessage(message.createdAt)}
          </span>
        </div>
        <button
          className="message-edit"
          onClick={() => editMessage(message.id)}
        >
          &#9998;
        </button>
        <button
          className="message-delete"
          onClick={() => deleteMessage(message.id)}
        >
          &#10006;
        </button>
      </div>
    );
  }
}

export default OwnMessage;
