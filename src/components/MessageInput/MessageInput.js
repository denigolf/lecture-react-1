import React, { Component } from 'react';
import './MessageInput.scss';

class MessageInput extends Component {
  constructor(props) {
    super(props);

    this.state = {
      inputValue: '',
    };

    this.changeInputHandler = this.changeInputHandler.bind(this);
    this.clearInput = this.clearInput.bind(this);
  }

  changeInputHandler(e) {
    const inputValue = e.target.value;
    this.setState({ inputValue });
  }

  clearInput() {
    this.setState({ inputValue: '' });
  }

  render() {
    const { sendMessageHandler } = this.props;

    return (
      <div className="message-input">
        <input
          className="message-input-text"
          type="text"
          placeholder="Enter your message..."
          onChange={this.changeInputHandler.bind(this)}
          value={this.state.inputValue}
        />
        <button
          className="message-input-button"
          onClick={() =>
            sendMessageHandler(this.state.inputValue, this.clearInput)
          }
        >
          Send
        </button>
      </div>
    );
  }
}

export default MessageInput;
